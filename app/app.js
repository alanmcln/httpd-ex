var crApp = angular.module('crApp', ["ngClickCopy"]);

crApp.controller('mainController', ['$scope','$http','$log','$templateCache', function($scope,$http,$log,$templateCache) {
	$scope.snowURLFixed="https://sim.srv.allianz/nav_to.do?uri=change_request.do?sys_id=-1%26sysparm_query=";
	//$scope.selectedCategory = "";
	//$scope.selectedClassification = "";
	//$scope.selectedState.id = "";
	//$scope.selectedAssignmentGroup.id= "";
	//$scope.selectedChangeOwner.id = "";
	$scope.shortDescription = ""; 
	$scope.description = ""; 
	$scope.justification = "";
	$scope.changePlan = "";
	$scope.backoutPlan = "";
	$scope.testPlan = "";
	$scope.method = 'GET';
	$scope.url = 'options/categories/categories.json';
    $http({method: $scope.method, url: $scope.url, cache: $templateCache}).
      then(function(response) {
        $scope.status = response.status;
        $scope.categories = response.data;
		$scope.selectedCategory = $scope.categories[0];
		//$scope.categoryValue = "category=" + $scope.selectedCategory;
      }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
		//$scope.categoryValue = "";
    });
	$scope.url = 'options/classifications/classifications.json';
    $http({method: $scope.method, url: $scope.url, cache: $templateCache}).
      then(function(response) {
        $scope.status = response.status;
        $scope.classifications = response.data;
		$scope.selectedClassification = $scope.classifications[0];
		//$scope.classificationValue = "^u_risk_class=" + $scope.selectedClassification;
      }, function(response) {
        $scope.data = response.data || 'Request failed'; 
        $scope.status = response.status;
		//$scope.classificationValue = "";
    });
	$scope.url = 'options/states/states.json';
    $http({method: $scope.method, url: $scope.url, cache: $templateCache}).
      then(function(response) {
        $scope.status = response.status;
        $scope.states = response.data;
		$scope.selectedState = $scope.states[0];
		//$scope.stateValue = "^state=" + $scope.selectedState.id;
      }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
		//$scope.stateValue = "";
    });
	$scope.url = 'options/assignmentgroups/assignmentgroups.json';
    $http({method: $scope.method, url: $scope.url, cache: $templateCache}).
      then(function(response) {
        $scope.status = response.status;
        $scope.assignmentGroups = response.data;
		$scope.selectedAssignmentGroup = $scope.assignmentGroups[0];
		//$scope.assignmentGroupIdValue = "^assignment_group=" + $scope.selectedAssignmentGroup.id;
      }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
		//$scope.assignmentGroupIdValue = "";
    });
	$scope.url = 'options/changeowners/changeowners.json';
    $http({method: $scope.method, url: $scope.url, cache: $templateCache}).
      then(function(response) {
        $scope.status = response.status;
        $scope.changeOwners = response.data;
		$scope.selectedChangeOwner = $scope.changeOwners[0];
		//$scope.changeOwnerIdValue = "^u_change_owner" + $scope.selectedChangeOwner.id;
      }, function(response) {
        $scope.data = response.data || 'Request failed';
        $scope.status = response.status;
		//$scope.changeOwnerIdValue = "";
    });
    $scope.makeSnowURL = function() {
		return $scope.snowURLFixed + 
			"category=" + $scope.selectedCategory + 
			"^u_risk_class=" + $scope.selectedClassification +
			"^state=" + $scope.selectedState.id +
			"^assignment_group=" + $scope.selectedAssignmentGroup.id +
			"^u_change_owner=" + $scope.selectedChangeOwner.id +
			"^short_description=" + $scope.shortDescription + 
			"^description=" + $scope.description + 
			"^justification" + $scope.justification +
			"^change_plan=" + $scope.changePlan +
			"^backout_plan=" + $scope.backoutPlan +
			"^test_plan=" + $scope.testPlan;
	}
}]);